
Type T_Adresse : < ville : string, cp : integer, rue : varchar >

Type T_Proprietaire : < login : string, nom : string, prenom : string, age : integer, photo : string, pseudo : integer , adresse : T_Adresse>
Proprietaire de T_Proprietaire (#login) avec adresse NOT NULL

Type T_Categorie : < nom : string >
Categorie de T_Categorie (#nom)
Type T_Modele : < nom : string >
Modele de T_Modele (#nom)
Type T_Carburant : < nom : string >
Carburant de T_Carburant (#nom)

Type T_Voiture : < numero_immatriculation : string, marque : string, couleur : string, annee_circulation : time, numero_contrat_assurance : integer, description : string, categorie =>o Categorie, modele =>o Modele, carburant =>o Carburant>
Voiture de T_Voiture (#numero_immatriculation) avec categorie, modele, carburant NOT NULL

Type T_Option : < nom : string, descript : sting >
tOption de T_Option (#nom)
Option_Voiture ( toption =>o tOption, voiture =>o Voiture )

Type T_Pays : < nom : string, descript : sting >
Pays de T_Pays (#nom)
Pays_Voiture ( pays =>o Pays, voiture =>o Voiture )

Annonce (prix : real, date_dispo_start : date, date_dispo_fin : date, kilometrage : integer, seuil_kilométrage : integer, niveau_carburant : real, prix_kilometrage_supp : real, prix_litre_supp : real, proprietaire =>o Proprietaire, voiture =>o Voiture )

Type T_Locataire : < login : string, nom : string, prenom : string, age : integer, photo : string, pseudo : integer , adresse : T_Adresse>
Locataire de T_Locataire (#login) avec adresse NOT NULL

Type T_Particulier hérite de T_Locataire <numéro_permis : integer, date_expiration : date>
Particulier de T_Particulier (#login) avec adresse NOT NULL

Type T_Entreprise hérite de T_Locataire <nomentreprise : string,nb_salarié : integer>
Entreprise de T_Entreprise (#login) avec adresse NOT NULL

Type T_Conducteur : <nom : string, prénom : string, entreprise =>o Entreprise>
Conducteur de T_Conducteur (#nom, #prénom)

Type T_Facture : < numero_facture : integer, kilometrage : integer, datefacture : date, moyen_paiement : {chèque, carte, liquide}, cout_horsforfait : real, =Calcul() real>
Facture de T_Facture (#numero-facture) 

Type T_Contrat_Location : <numéro_contrat : integer, photo_véhicule : string, choix_franchises : {sans réduction, franchise réduite, zéro franchise}, Date_début : date, Date_fin : date, facture =>o Facture, particulier =>o Particulier, entreprise =>o Entreprise>
Contrat_Location de T_Contrat_Location (#numéro_contrat) avec facture NOT NULL

Avis (commentaire : string, note : integer, signalement : boolean, contrat =>o Contrat_Location, locataire =>o Locataire)

Critere (#nom : string, check_in : boolean, check_out : boolean, photo : string, contrat =>o Contrat_Location)
