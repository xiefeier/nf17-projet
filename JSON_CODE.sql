-- considerer adresse comme type json
create table PROPRIETAIRE
(
  LOGIN text primary key,	
  NOM text,
  PRENOM text,
  AGE integer,
  PHOTO text,
  PSEUDO text,
  adresse json
);

create table LOCATAIRE
(
  LOGIN text primary key,	
  NOM text,
  PRENOM text,
  AGE integer,
  PHOTO text,
  PSEUDO text,
  adresse json
);

create table PARTICULIER
(
  LOGIN text primary key,	
  DATE_EXPIRATION date,
  NUMERO_PERMIS integer NOT NULL UNIQUE,
  foreign key (LOGIN) references LOCATAIRE(login)
);



--imbriquer les infos de conducteurs dans le table entreprise
create table ENTREPRISE
(
  LOGIN text primary key,	
  NOM text NOT NULL UNIQUE,
  NB_SALARIES integer,
  CONDUCTEURS json,
  foreign key (LOGIN) references LOCATAIRE(login)
);

create table MODELE
(
  NOM text primary key
);

create table CARBURANT
(
  NOM text primary key	
);

create table CATEGORIE
(
  NOM text primary key	
);



--imbriquer les infos de option, pays comme type json (multivalués)
create table VOITURE
(
  NUMERO_IMMATRICULATION text primary key,	
  MARQUE text,
  COULEUR text,
  ANNEE_CIRCULATION integer,
  NUMERO_CONTRAT_ASSURANCE integer,
  DESCRIPTION text, 
  MODELE text not null, 
  CARBURANT text not null, 
  CATEGORIE text not null, 
  OPTION json,
  PAYS json,
  foreign key (MODELE) references MODELE(NOM),
  foreign key (CARBURANT) references CARBURANT(NOM),
  foreign key (CATEGORIE) references CATEGORIE(NOM)
);



create table ANNONCE
(
  PROPRIETAIRE text,	
  VOITURE text,
  PRIX real,
  DATE_DISPO_START date,
  DATE_DISPO_FIN date,
  KILOMETRAGE integer, 
  SEUIL_KILOMETRAGE integer, 
  NIVEAU_CARBURANT real, 
  PRIX_KILOMETRAGE_SUPP real,
  PRIX_LITRE_SUPP real, 
  primary key (PROPRIETAIRE,VOITURE),
  foreign key (PROPRIETAIRE) references PROPRIETAIRE(LOGIN),
  foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION)
);



-- imbriquer les infos de facture et critere dans le table contrat_location comme type json

create table CONTRAT_LOCATION
(
    NUMERO_CONTRAT integer primary key,
    PHOTO_VEHICULE varchar,
    CHOIX_FRANCHISES varchar,
    CHECK(CHOIX_FRANCHISES='SANS REDUCTION' OR CHOIX_FRANCHISES='FRANCHISE REDUITE' OR CHOIX_FRANCHISES='ZERO FRANCHISE'),
    DATE_DEBUT date,
    DATE_FIN date,
    VOITURE varchar,
    ENTREPRISE varchar,
    PARTICULIER varchar,
    FACTURE json,
    CRITERE json,
    foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION),
    foreign key (ENTREPRISE) references ENTREPRISE(LOGIN),
    foreign key (PARTICULIER) references PARTICULIER(LOGIN)
);



create table AVIS
(
    CONTRAT integer,
    LOCATAIRE varchar,
    COMMENTAIRE varchar,
    NOTE integer,
    SIGNALEMENT boolean,
    foreign key (CONTRAT) references CONTRAT_LOCATION(NUMERO_CONTRAT),
    foreign key (LOCATAIRE) references LOCATAIRE(login),
    PRIMARY KEY (CONTRAT, LOCATAIRE),
    CHECK (NOTE BETWEEN 1 AND 5)
);



---insert
INSERT INTO PROPRIETAIRE(login, nom, prenom, age, photo, pseudo, adresse)
    VALUES ('AntoineL', 'LEBRUN', 'Antoine', 21, 'https://xxxx.com/Antoine/photos-id.jpg','Antho','{"ville":"Compiegne", "cp":60200, "rue":"rue de A"}');
    
INSERT INTO PROPRIETAIRE(login, nom, prenom, age, photo, pseudo, adresse)
    VALUES ('GuillaumeR', 'RENAUD', 'Guillaume', 22, 'https://xxxx.com/Guillaume/photos-id.jpg','Guigui','{"ville":"Paris", "cp":65001, "rue":"rue de B"}');

INSERT INTO LOCATAIRE(login, nom, prenom, age, photo, pseudo, adresse)
VALUES ('HugoK', 'KESSLER', 'Hugo', 23, 'https://xxxx.com/Hugo/photos-id.jpg','Hug', '{"ville":"Lyon", "cp":69001, "rue":"rue de C"}');
    
INSERT INTO LOCATAIRE(login, nom, prenom, age, photo, pseudo, adresse)
VALUES ('FeierX', 'XIE', 'Feier', 24, 'https://xxxx.com/Feier/photos-id.jpg','Feifei', '{"ville":"Bordeaux", "cp":33100, "rue":"rue de D"}');

INSERT INTO LOCATAIRE(login,  pseudo, adresse)
VALUES ('SG', 'Societeg', '{"ville":"Toulouse", "cp":31000, "rue":"rue de E"}');

INSERT INTO LOCATAIRE(login,  pseudo, adresse)
VALUES ('RN', 'Ren', '{"ville":"Paris", "cp":65001, "rue":"rue de F"}');

INSERT INTO ENTREPRISE(login,  nom, nb_salaries, conducteurs)
VALUES ('SG', 'Societe General', 200, '[
{"nom":"X", "prenom":"Clara"},
{"nom":"Y", "prenom":"Julie"}
]' );

INSERT INTO ENTREPRISE(login,  nom, nb_salaries, conducteurs)
VALUES ('RN', 'Renault', 250, '[
{"nom":"H", "prenom":"Clement"},
{"nom":"S", "prenom":"Lucie"}
]');

INSERT INTO PARTICULIER(login,  numero_permis, date_expiration)
VALUES ('HugoK', 911091204, TO_DATE('2020-03-03','YYYY-MM-DD'));   
    
INSERT INTO PARTICULIER(login,  numero_permis, date_expiration)
VALUES ('FeierX', 961092804, TO_DATE('2023-07-13','YYYY-MM-DD'));

INSERT INTO MODELE(nom)
VALUES ('C4');

INSERT INTO MODELE(nom)
VALUES ('Serie 5');

INSERT INTO MODELE(nom)
VALUES ('Duster');

INSERT INTO MODELE(nom)
VALUES ('Mustang');

INSERT INTO CARBURANT(nom)
VALUES ('SP95');

INSERT INTO CARBURANT(nom)
VALUES ('Electrique');

INSERT INTO CARBURANT(nom)
VALUES ('GPL');

INSERT INTO CATEGORIE(nom)
VALUES ('4x4');

INSERT INTO CATEGORIE(nom)
VALUES ('Sportive');

INSERT INTO CATEGORIE(nom)
VALUES ('Citadine');


INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie, option, pays)
VALUES ('FF-277-CK', 'CITROEN','rouge', 1997, 100,'C4', 'SP95', 'Citadine',
'[{"nom":"GPS", "description":"gps"}, {"nom":"Siege chauffant", "description":"2 sièges avant chauffant, 2 températures"}]', '["Chine","France"]');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie, option, pays)
VALUES ('FF-238-CJ', 'DACIA','noir', 2000, 101,'Duster', 'SP95', '4x4',
'{"nom":"Kit main libre", "description":"Kit main libre"}', '["Italie","France"]');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie, option, pays)
VALUES ('FF-179-CK', 'BMW','gris', 2001, 102,'Serie 5', 'SP95', 'Sportive',
'[{"nom":"GPS", "description":"gps"}, {"nom":"Kit main libre", "description":"Kit main libre"}]', '["Espagne","France"]');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie, option, pays)
VALUES ('FF-627-CJ', 'FORD','noir', 1987, 103,'Mustang', 'GPL', 'Sportive',
'{"nom":"GPS", "description":"gps"}', '["Italie","France"]');

INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('AntoineL', 'FF-277-CK', 300.5,TO_DATE('2019-01-01','YYYY-MM-DD'),TO_DATE('2019-10-10','YYYY-MM-DD'),122,200,0.67,10.55,23.27);

INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('GuillaumeR', 'FF-627-CJ', 500,TO_DATE('2019-03-06','YYYY-MM-DD'),TO_DATE('2019-06-07','YYYY-MM-DD'),12546,12873,0.89,20.35,13.28);

INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('GuillaumeR', 'FF-179-CK', 200,TO_DATE('2018-01-01','YYYY-MM-DD'),TO_DATE('2017-07-07','YYYY-MM-DD'),167,189,0.88,25.45,10.55);

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,voiture, entreprise, facture, critere)
VALUES (2000, 'https://xxxx.com/voiture1/photos.jpg','SANS REDUCTION', TO_DATE('2018-03-04','YYYY-MM-DD'),TO_DATE('2018-03-15','YYYY-MM-DD'),'FF-277-CK', 'SG',
'{"numero_facture":1, "kilometrage":145, "date":"2018-03-15", "cout_hors_forfait":0.00,"moyen_de_paiement":"CHEQUE"}',
'{"nom":"Siege", "check_in":"true", "check_out":"true", "photo":"https://xxxx.com/critere1/photos.jpg"}');

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,voiture, particulier, facture, critere)
VALUES (2001, 'https://xxxx.com/voiture2/photos.jpg','FRANCHISE REDUITE', TO_DATE('2018-03-15','YYYY-MM-DD'),TO_DATE('2018-07-08','YYYY-MM-DD'),'FF-627-CJ', 'HugoK',
'{"numero_facture":2, "kilometrage":27839, "date":"2018-07-08", "cout_hors_forfait":10.5,"moyen_de_paiement":"CARTE"}',
'{"nom":"Volant", "check_in":"true", "check_out":"true", "photo":"https://xxxx.com/critere2/photos.jpg"}');

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,voiture, particulier, facture, critere)
VALUES (2002, 'https://xxxx.com/voiture3/photos.jpg','ZERO FRANCHISE', TO_DATE('2018-07-08','YYYY-MM-DD'),TO_DATE('2018-11-12','YYYY-MM-DD'),'FF-179-CK', 'FeierX',
'{"numero_facture":3, "kilometrage":179, "date":"2018-12-08", "cout_hors_forfait":13.5,"moyen_de_paiement":"LIQUIDE"}',
'{"nom":"Volant", "check_in":"true", "check_out":"false", "photo":"https://xxxx.com/critere3/photos.jpg"}');

INSERT INTO AVIS(contrat, locataire, commentaire, note, signalement)
VALUES (2000, 'SG', 'bien',2,TRUE);

INSERT INTO AVIS(contrat, locataire, commentaire, note, signalement)
VALUES (2001, 'FeierX', 'bon',5,FALSE);

--requette
--afficher les numero_immatriculation des voitures avec leurs pays autorisé
SELECT v.numero_immatriculation, p.*
FROM Voiture v, JSON_ARRAY_ELEMENTS(pays) p;

--afficher les proprietaire avec leurs adresses
SELECT p.nom, p.prenom, CAST(ad->>'cp' AS INTEGER) AS cp, ad->>'rue' AS rue, ad->>'ville' AS ville
FROM proprietaire p, JSON_ARRAY_ELEMENTS(adresse) ad;

--afficher les numeros de contrat avec les infos de facture
SELECT cl.numero_contrat, f.* 
FROM contrat_location cl, JSON_TO_RECORDSET(facture) f (numero_facture INTEGER, kilometrage INTEGER, date TEXT, cout_hors_forfait real, moyen_de_paiement TEXT);
