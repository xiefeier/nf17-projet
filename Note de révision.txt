1. Preuve de normalisation 

Notre modèle  est clairement en BCNF, car les seules DFE existantes sont de la forme K -> A où K est une clé.

Autre explication :

Notre modélisation est déjà en 1NF car tous les attributs sont atomique. De plus, tous les clés des tableaux contient un seule attribut sauf ‘conducteur’. Pour ‘conducteur’ aucun attributs non-clé détermine ‘nom’ ou ‘prénom’, donc notre modélisation est aussi en 2NF. Et tout attribut n’appartenant pas à  aucune clé candidate ne dépend directement que de clés candidates.


2. Ajouter des contraintes sur les forme les attributs ( ex : limiter le note entre 1 et 5 - ajouter CHECK (NOTE BETWEEN 1 AND 5))

3. Ajouter une vue pour détecter les cas négatif de la contraint {XOR} entre particulier- entreprise ( pour afficher les clients qui sont les particuliers mais à la fois un entreprise )


4. Ajouter des codes pour gérer les doits des utilisateurs




Livrable 6 : ( les codes d’optimisation sont aussi dans le fichier Livrable_6.sql )

Requête 1 : Sélectionner un véhicule étant donné une région et projection de l'ensemble des informations. Cette requête doit être le plus performant possible.

Requête 2 : Sélectionner un contrat avec toutes les informations la concernant (états des lieu entrée/sortie, franchise, véhicule). Le recours à cette table est fréquent pendant des opérations internes à l'entreprise (audits, organisation...).

Nous avons aussi proposé des autres requêtes dont les résultats sont dans les vues 1/2/3/4 ( voir la partie vues dans le fichier SQL_LDD.sql )

—— Optimisation de requête 1 : —

Pour cette requête, on peut faire un partitionnement horizontal de la table ‘Conduite_Autorisée’ par pays ( créer trois nouveaux tableaux VOITURE_FRANCE, VOITURE_ITALIE, VOITURE_CHINE ). Mais ça va ralentir la requête 2.

exemple de requête :
Afficher les infos de voitures autorisées en France :

select *
from VOITURE v join VOITURE_FRANCE f
on v.numero_immatriculation = f.voiture ;

code d’optimisation :

create table VOITURE_FRANCE
 (
   VOITURE varchar
 );

insert into VOITURE_FRANCE
select voiture
from CONDUITE_AUTORISEE
where pays = 'France' ;


create table VOITURE_ITALIE
 (
   VOITURE varchar
 );

insert into VOITURE_ITALIE
select voiture
from CONDUITE_AUTORISEE
where pays = 'Italie' ;

create table VOITURE_CHINE
 (
   VOITURE varchar
 );

insert into VOITURE_CHINE
select voiture
from CONDUITE_AUTORISEE
where pays = 'Chine' ;


— Optimisation de requête 2 : — 

Pour cette requête, on peut créer une vue matérialisée INFO_CONTRAT qui contient tous les informations de chaque contrat afin d’afficher les résultats rapidement.

exemple de requête :
Afficher les infos de contrat numero ‘2001’ :

select *
from INFO_CONTRAT
where numero_contrat = 2001 ;

code d’optimisation:
create view INFO_CONTRAT as
select c.numero_contrat, c.choix_franchises, c.voiture, cr.check_in, cr.check_out
from CONTRAT_LOCATION c, CRITERE cr
where c.numero_contrat = cr.contrat ;
