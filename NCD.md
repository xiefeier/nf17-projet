## NCD de sociéte vehicleShare 

Cette note de clarification est une récriture du cahier des charges trouvable à cette adresse : https://librecours.net/projet/projets-19p-nx17/projets-19p-nx17_1.xhtml.

L'objectif du projet est de réaliser une application de location de véhicule en ligne de particulier à particulier. 

#### Les clients : 

Les clients ont les attributs suivants : 
+ Nom
+ Prénom
+ Age
+ Photo
+ Pseudo
+ Coordonnées

Un client peut être, conjointement :
+ Un locataire
+ Un propriétaire

Pour les locataires, VehicleShare demande le numéro de permis de conduire ainsi que sa date d’expiration, pour vérifier si le permis est valide pendant toute la période de location. 
Un locataire ne peut que louer un voiture chaque fois.
Un locataire peut également être une entreprise, auquel cas elle devra entrer des informations spécifiques aux entreprises ainsi que les salariés susceptibles d’utiliser les véhicules loués. Une entreprise peut louer plusieurs véhicules en même temps.
On donne aussi à chaque client un login et un mot de passe pour gérer les comptes et la sécurité. Les clients sont identifiés par le login car deux clients ne peuvent pas avoir le même login. 

#### Les véhicules:

L’entreprise demande les informations suivantes sur les véhicules :
+ Numéro d'immatriculation 
+ Catégorie
+ Marque
+ Modèle
+ Couleur
+ Carburant
+ Options
+ Année de mise en circulation
+ Contrat d’assurance
+ Liste des pays dans lequel le véhicule peut circuler
+ Période de disponibilité du véhicule
+ Brève description
+ Nombre d’être signalé

#### Les locations (commandes) :

Fonctionnalité principale de l’application, entre un locataire et un propriétaire.
+ Numéro unique de contrat de location
+ Photos du véhicule prises pendant l’état des lieux
+ Check-liste remplie pendant l’état des lieux pour signaler le moindre dégât sur le véhicule
+ un état de lieu afin de résoudre les problèmes en cas d’accident. Ils enregistrent aussi le choix du locataire pour la réduction de franchise (sans réduction, franchise réduite et Zéro franchise)
+ Une location est close lorsque le check-out (état des lieux du propriétaire) est effectué. Ce check-out peut donner suite à des demandes de dédommagement

#### Les annonces :

Les annonces, rédigées par le propriétaires, doivent respecter les conditions ci-après :
+ Chaque annonce concerne un seul véhicule
+ Comporte un prix 
+ Comporte une période disponible
+ Comporte le kilométrage du véhicule
+ Comporte un seuil de kilométrage
+ Comporte le niveau de carburant du véhicule
+ Le prix du kilomètre supplémentaire (x dans le cahier des charge)
+ Le prix du litre supplémentaire (y dans le cahier des charges)

#### Facture : 

La facture est crée suite à un contrat. Elle comporte les mentions suivantes :
+ Kilométrage
+ Date
+ Moyen de paiement 
+ Coûts générés par le hors forfait (kilomètre ou litre supplémentaire)
+ Prix

#### Commentaire (note) : 

Les locataires peuvent commenter, noter (note de 0 à 5) ou signaler un véhicule.


