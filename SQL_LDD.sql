create table PROPRIETAIRE
(
  LOGIN varchar primary key,	
  NOM varchar,
  PRENOM varchar,
  AGE integer,
  PHOTO varchar,
  PSEUDO varchar,
  VILLE varchar,
  CP integer,
  RUE varchar
);

create table LOCATAIRE
(
  LOGIN varchar primary key,	
  NOM varchar,
  PRENOM varchar,
  AGE integer,
  PHOTO varchar,
  PSEUDO varchar,
  VILLE varchar,
  CP integer,
  RUE varchar
);

create table PARTICULIER
(
  LOGIN varchar primary key,	
  DATE_EXPIRATION date,
  NUMERO_PERMIS integer NOT NULL UNIQUE,
  foreign key (LOGIN) references LOCATAIRE(login)
);

create table ENTREPRISE
(
  LOGIN varchar primary key,	
  NOM varchar NOT NULL UNIQUE,
  NB_SALARIES integer,
  foreign key (LOGIN) references LOCATAIRE(login)
);

create table MODELE
(
  NOM varchar primary key
);

create table CARBURANT
(
  NOM varchar primary key	
);

create table CATEGORIE
(
  NOM varchar primary key	
);

create table OPTION
(
  NOM varchar primary key,	
  DESCRIPTION varchar 
);
create table PAYS
(
  NOM varchar primary key	
);

create table VOITURE
(
  NUMERO_IMMATRICULATION varchar primary key,	
  MARQUE varchar,
  COULEUR varchar,
  ANNEE_CIRCULATION integer,
  NUMERO_CONTRAT_ASSURANCE integer,
  DESCRIPTION varchar, 
  MODELE varchar not null, 
  CARBURANT varchar not null, 
  CATEGORIE varchar not null, 
  foreign key (MODELE) references MODELE(NOM),
  foreign key (CARBURANT) references CARBURANT(NOM),
  foreign key (CATEGORIE) references CATEGORIE(NOM)
);



create table ANNONCE
(
  PROPRIETAIRE varchar,	
  VOITURE varchar,
  PRIX real,
  DATE_DISPO_START date,
  DATE_DISPO_FIN date,
  KILOMETRAGE integer, 
  SEUIL_KILOMETRAGE integer, 
  NIVEAU_CARBURANT real, 
  PRIX_KILOMETRAGE_SUPP real,
  PRIX_LITRE_SUPP real, 
  constraint PK_ANNONCE primary key (PROPRIETAIRE,VOITURE),
  foreign key (PROPRIETAIRE) references PROPRIETAIRE(LOGIN),
  foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION)
);

create table OPTIONS
(
  OPTION varchar,	
  VOITURE varchar, 
  constraint PK_OPTIONS primary key (OPTION,VOITURE),
  foreign key (OPTION) references OPTION(NOM),
  foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION)
);


create table CONDUITE_AUTORISEE
 (
   PAYS varchar,
   VOITURE varchar,
   constraint PK_CONDUITE primary key (PAYS,VOITURE),
   foreign key (PAYS) references PAYS(NOM),
   foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION)
 );


create table FACTURE 
(
    NUMERO_FACTURE integer PRIMARY KEY,
    KILOMETRAGE integer,
    DATE date,
    COUT_HORS_FORFAIT real,
    MOYEN_DE_PAIEMENT varchar,
    CHECK(MOYEN_DE_PAIEMENT ='CHEQUE' OR MOYEN_DE_PAIEMENT ='CARTE' OR MOYEN_DE_PAIEMENT ='LIQUIDE')
);

create table CONTRAT_LOCATION
(
    NUMERO_CONTRAT integer primary key,
    PHOTO_VEHICULE varchar,
    CHOIX_FRANCHISES varchar,
    CHECK(CHOIX_FRANCHISES='SANS REDUCTION' OR CHOIX_FRANCHISES='FRANCHISE REDUITE' OR CHOIX_FRANCHISES='ZERO FRANCHISE'),
    DATE_DEBUT date,
    DATE_FIN date,
    FACTURE integer,
    VOITURE varchar,
    ENTREPRISE varchar,
    PARTICULIER varchar,
    foreign key (FACTURE) references FACTURE(NUMERO_FACTURE),
    foreign key (VOITURE) references VOITURE(NUMERO_IMMATRICULATION),
    foreign key (ENTREPRISE) references ENTREPRISE(LOGIN),
    foreign key (PARTICULIER) references PARTICULIER(LOGIN)
);



create table AVIS
(
    CONTRAT integer,
    LOCATAIRE varchar,
    COMMENTAIRE varchar,
    NOTE integer,
    SIGNALEMENT boolean,
    foreign key (CONTRAT) references CONTRAT_LOCATION(NUMERO_CONTRAT),
    foreign key (LOCATAIRE) references LOCATAIRE(login),
    PRIMARY KEY (CONTRAT, LOCATAIRE),
    CHECK (NOTE BETWEEN 1 AND 5)
);


create table CONDUCTEUR
(
    NOM varchar,
    PRENOM varchar,
    ENTREPRISE varchar,
    constraint PK_CONDUCTEUR primary key (NOM,PRENOM),
    foreign key (ENTREPRISE) references ENTREPRISE(login)
);


create table CRITERE
(
    NOM varchar PRIMARY KEY,
    CHECK_IN boolean,
    CHECK_OUT boolean,
    PHOTO varchar,
    CONTRAT integer,
    foreign key (CONTRAT) references CONTRAT_LOCATION(NUMERO_CONTRAT)
);



-- vue1 ： stock tous les infos de clients
create view vCLIENT as 
SELECT login, nom, prenom, age, photo, pseudo, ville, cp, rue FROM proprietaire
UNION ALL
SELECT login, nom, prenom, age, photo, pseudo, ville, cp, rue FROM locataire;

-- vue2 : nombre de voiture loués par entreprise
create view prixEntreprises as
SELECT e.nom as entreprise, COUNT(c.voiture) as nb_voiture
FROM ENTREPRISE e, CONTRAT_LOCATION c
WHERE e.login = c.entreprise
GROUP BY e.nom;

-- vue3 : moyen de note par voiture (ordre croissant)
create view moyennote as
SELECT c.voiture as Voiture, AVG(a.note) as Moyen_note
FROM CONTRAT_LOCATION c, AVIS a
WHERE c.NUMERO_CONTRAT = a.CONTRAT
GROUP BY c.voiture
ORDER BY Moyen_note;

-- vue4 : nombre d'être signalé par voiture
create view nb_signale as
SELECT c.voiture as Voiture, COUNT(a.signalement) as nb_signalement
FROM CONTRAT_LOCATION c, AVIS a
WHERE c.NUMERO_CONTRAT = a.CONTRAT
AND a.signalement = TRUE
GROUP BY c.voiture
ORDER BY nb_signalement;

-- vue5 : une vue pour détecter les cas négatif de la contraint {XOR} entre particulier- entreprise
SELECT entreprise, particulier
FROM CONTRAT_LOCATION
WHERE (entreprise IS NULL and particulier IS NULL) or (entreprise IS not NULL and particulier IS not NULL);

INSERT INTO PROPRIETAIRE(login, nom, prenom, age, photo, pseudo, ville, cp, rue)
    VALUES ('AntoineL', 'LEBRUN', 'Antoine', 21, 'https://xxxx.com/Antoine/photos-id.jpg','Antho', 'Compiegne', 60200,'rue de A');
    
INSERT INTO PROPRIETAIRE(login, nom, prenom, age, photo, pseudo, ville, cp, rue)
    VALUES ('GuillaumeR', 'RENAUD', 'Guillaume', 22, 'https://xxxx.com/Guillaume/photos-id.jpg','Guigui', 'Paris', 65001,'rue de B');
    
INSERT INTO LOCATAIRE(login, nom, prenom, age, photo, pseudo, ville, cp, rue)
VALUES ('HugoK', 'KESSLER', 'Hugo', 23, 'https://xxxx.com/Hugo/photos-id.jpg','Hug', 'Lyon', 69001,'rue de C');
    
INSERT INTO LOCATAIRE(login, nom, prenom, age, photo, pseudo, ville, cp, rue)
VALUES ('FeierX', 'XIE', 'Feier', 24, 'https://xxxx.com/Feier/photos-id.jpg','Feifei', 'Bordeaux', 33100,'rue de D');
   
INSERT INTO LOCATAIRE(login,  pseudo, ville, cp, rue)
VALUES ('SG', 'Societeg', 'Toulouse', 31000,'rue de E');

INSERT INTO LOCATAIRE(login,  pseudo, ville, cp, rue)
VALUES ('RN', 'Ren', 'Paris', 65001,'rue de F');

INSERT INTO PARTICULIER(login,  numero_permis, date_expiration)
VALUES ('HugoK', 911091204, TO_DATE('2020-03-03','YYYY-MM-DD'));   
    
INSERT INTO PARTICULIER(login,  numero_permis, date_expiration)
VALUES ('FeierX', 961092804, TO_DATE('2023-07-13','YYYY-MM-DD'));

INSERT INTO ENTREPRISE(login,  nom, nb_salaries)
VALUES ('SG', 'Societe General', 200);

INSERT INTO ENTREPRISE(login,  nom, nb_salaries)
VALUES ('RN', 'Renault', 250);

INSERT INTO MODELE(nom)
VALUES ('C4');

INSERT INTO MODELE(nom)
VALUES ('Serie 5');

INSERT INTO MODELE(nom)
VALUES ('Duster');

INSERT INTO MODELE(nom)
VALUES ('Mustang');

INSERT INTO CARBURANT(nom)
VALUES ('SP95');

INSERT INTO CARBURANT(nom)
VALUES ('Electrique');

INSERT INTO CARBURANT(nom)
VALUES ('GPL');

INSERT INTO CATEGORIE(nom)
VALUES ('4x4');

INSERT INTO CATEGORIE(nom)
VALUES ('Sportive');

INSERT INTO CATEGORIE(nom)
VALUES ('Citadine');

INSERT INTO PAYS(nom)
VALUES ('France');

INSERT INTO PAYS(nom)
VALUES ('Chine');

INSERT INTO PAYS(nom)
VALUES ('Italie');

INSERT INTO PAYS(nom)
VALUES ('Allemagne');

INSERT INTO OPTION(nom, description)
VALUES ('GPS', 'gps');

INSERT INTO OPTION(nom, description)
VALUES ('Siege chauffant', '2 sièges avant chauffant, 2 températures');

INSERT INTO OPTION(nom, description)
VALUES ('Kit main libre', 'Kit main libre');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie)
VALUES ('FF-277-CK', 'CITROEN','rouge', 1997, 100,'C4', 'SP95', 'Citadine');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie)
VALUES ('FF-238-CJ', 'DACIA','noir', 2000, 101,'Duster', 'SP95', '4x4');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie)
VALUES ('FF-179-CK', 'BMW','gris', 2001, 102,'Serie 5', 'SP95', 'Sportive');

INSERT INTO VOITURE(numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, modele, carburant,categorie)
VALUES ('FF-627-CJ', 'FORD','noir', 1987, 103,'Mustang', 'GPL', 'Sportive');


INSERT INTO OPTIONS(option, voiture)
VALUES ('GPS', 'FF-238-CJ');

INSERT INTO OPTIONS(option, voiture)
VALUES ('GPS', 'FF-179-CK');

INSERT INTO OPTIONS(option, voiture)
VALUES ('Kit main libre', 'FF-277-CK');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('France', 'FF-277-CK');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('Chine', 'FF-277-CK');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('Italie', 'FF-277-CK');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('France', 'FF-238-CJ');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('France', 'FF-179-CK');

INSERT INTO CONDUITE_AUTORISEE(pays, voiture)
VALUES ('France', 'FF-627-CJ');

INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('AntoineL', 'FF-277-CK', 300.5,TO_DATE('2019-01-01','YYYY-MM-DD'),TO_DATE('2019-10-10','YYYY-MM-DD'),122,200,0.67,10.55,23.27);


INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('GuillaumeR', 'FF-627-CJ', 500,TO_DATE('2019-03-06','YYYY-MM-DD'),TO_DATE('2019-06-07','YYYY-MM-DD'),12546,12873,0.89,20.35,13.28);

INSERT INTO ANNONCE(proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilometrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp)
VALUES ('GuillaumeR', 'FF-179-CK', 200,TO_DATE('2018-01-01','YYYY-MM-DD'),TO_DATE('2017-07-07','YYYY-MM-DD'),167,189,0.88,25.45,10.55);

INSERT INTO FACTURE(numero_facture, kilometrage, date, cout_hors_forfait,moyen_de_paiement)
VALUES (1, 145, TO_DATE('2018-03-15','YYYY-MM-DD'),0.00,'CHEQUE');

INSERT INTO FACTURE(numero_facture, kilometrage, date, cout_hors_forfait,moyen_de_paiement)
VALUES (2, 27839, TO_DATE('2018-07-08','YYYY-MM-DD'),10.5,'CARTE');

INSERT INTO FACTURE(numero_facture, kilometrage, date, cout_hors_forfait,moyen_de_paiement)
VALUES (3, 178, TO_DATE('2018-11-12','YYYY-MM-DD'),13.28,'LIQUIDE');

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,facture,voiture, entreprise)
VALUES (2000, 'https://xxxx.com/voiture1/photos.jpg','SANS REDUCTION', TO_DATE('2018-03-04','YYYY-MM-DD'),TO_DATE('2018-03-15','YYYY-MM-DD'),1,'FF-277-CK', 'SG');

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,facture,voiture, particulier)
VALUES (2001, 'https://xxxx.com/voiture2/photos.jpg','FRANCHISE REDUITE', TO_DATE('2018-03-15','YYYY-MM-DD'),TO_DATE('2018-07-08','YYYY-MM-DD'),2,'FF-627-CJ', 'HugoK');

INSERT INTO CONTRAT_LOCATION(numero_contrat, photo_vehicule, choix_franchises, date_debut,date_fin,facture,voiture, particulier)
VALUES (2002, 'https://xxxx.com/voiture3/photos.jpg','ZERO FRANCHISE', TO_DATE('2018-07-08','YYYY-MM-DD'),TO_DATE('2018-11-12','YYYY-MM-DD'),3,'FF-179-CK', 'FeierX');

INSERT INTO AVIS(contrat, locataire, commentaire, note, signalement)
VALUES (2000, 'SG', 'bien',2,TRUE);

INSERT INTO AVIS(contrat, locataire, commentaire, note, signalement)
VALUES (2001, 'FeierX', 'bon',5,FALSE);

INSERT INTO CONDUCTEUR(nom, prenom, entreprise)
VALUES ('HJS', 'Julie', 'SG');

INSERT INTO CONDUCTEUR(nom, prenom, entreprise)
VALUES ('LUI', 'Clement', 'RN');

INSERT INTO CRITERE(nom, check_in, check_out,photo,contrat)
VALUES ('Siege', TRUE, TRUE,'https://xxxx.com/critere1/photos.jpg',2000);

INSERT INTO CRITERE(nom, check_in, check_out,photo,contrat)
VALUES ('Volant', TRUE, TRUE,'https://xxxx.com/critere2/photos.jpg',2001);

-- Gestion des doits des utilisateurs
CREATE USER Entreprise;
CREATE USER Particulier;
CREATE USER Proprietaire;
 
GRANT SELECT, INSERT, DELETE, UPDATE ON Proprietaire TO Proprietaire;
GRANT SELECT, INSERT, DELETE, UPDATE ON Locataire TO Particulier;
GRANT SELECT, INSERT, DELETE, UPDATE ON Locataire TO Entreprise;
GRANT SELECT, INSERT, DELETE, UPDATE ON Particulier TO Particulier;
GRANT SELECT, INSERT, DELETE, UPDATE ON Entreprise TO Entreprise;
GRANT SELECT, INSERT, DELETE, UPDATE ON Voiture TO Proprietaire;  
GRANT SELECT, INSERT, UPDATE ON Pays TO Proprietaire;
GRANT SELECT, INSERT, DELETE, UPDATE ON Annonce TO Proprietaire;
GRANT SELECT, INSERT, UPDATE ON Contrat_Location TO Proprietaire;
GRANT SELECT, INSERT, UPDATE ON Contrat_Location TO Entreprise;
GRANT SELECT, INSERT, UPDATE ON Contrat_Location TO Particulier;
GRANT SELECT, INSERT, UPDATE ON Facture TO Proprietaire;
GRANT SELECT, INSERT, UPDATE ON Avis TO Particulier;
GRANT SELECT, INSERT, UPDATE ON Avis TO Entreprise;
GRANT SELECT, INSERT, DELETE, UPDATE ON Conducteur TO Entreprise;
GRANT SELECT, INSERT, UPDATE ON Critere TO Proprietaire;
GRANT SELECT, INSERT, UPDATE ON Critere TO Particulier;
GRANT SELECT, INSERT, UPDATE ON Critere TO Entreprise;
