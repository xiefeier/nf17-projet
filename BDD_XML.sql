--on n'applique que XMLTYPE pour la table Voiture 

CREATE TABLE Voiture (
	numero_immatriculation VARCHAR2(100),
	marque VARCHAR2(100),
	couleur VARCHAR2(100),
	annee_circulation NUMBER(4),
	numero_contrat_assurance NUMBER(4),
	description VARCHAR2(200),
	options XMLTYPE,
	pays XMLTYPE

);
/

INSERT INTO Voiture VALUES ('FF-277-CK','CITROEN','rouge',1997,100,'voiture1',
XMLTYPE(
	'
	<options>
		<option>
			<nom>GPS</nom>
			<description>gps</description>
		</option>
		<option>
			<nom>Siège chauffant</nom>
			<description>deux températures</description>
		</option>
	</options>
	'
),
XMLTYPE(
	'
	<pays>
		<nompays>Chine</nompays>
		<nompays>France</nompays>
	</pays>
	'
)
);
/

INSERT INTO Voiture VALUES ('FF-238-CJ','DACIA','noir',2000,101,'voiture2',
XMLTYPE(
	'
	<options>
		<option>
			<nom>Kit main libre</nom>
			<description>Kit main libre</description>
		</option>
		<option>
			<nom>Siège chauffant</nom>
			<description>deux températures</description>
		</option>
	</options>
	'
),
XMLTYPE(
	'
	<pays>
		<nompays>Italie</nompays>
		<nompays>Espagne</nompays>
	</pays>
	'
)
);
/

--requete 1 : la voiture FF-238-CJ est autorisé dans quels pays ?
SELECT v.pays.EXTRACT('/pays/nompays').GETSTRINGVAL() AS pays
FROM Voiture v
WHERE v.numero_immatriculation = 'FF-238-CJ';

--requete 2 : la voiture FF-277-CK ont quels options ?
SELECT v.options.EXTRACT('/options/option/nom').GETSTRINGVAL() AS options
FROM Voiture v
WHERE v.numero_immatriculation = 'FF-277-CK';




