--creat
CREATE TYPE T_Adresse AS OBJECT
(
	ville  VARCHAR2(20),
	cp  NUMBER(5),
	rue  varchar2(30)
);
/
CREATE TYPE T_Proprietaire AS OBJECT
(
    login VARCHAR2(20), 
    nom VARCHAR2(20), 
    prenom VARCHAR2(20),
    age NUMBER(3), 
    photo VARCHAR(200), 
    pseudo VARCHAR2(7),
    adresse  T_Adresse
);
/
CREATE TABLE Proprietaire OF T_Proprietaire 
(
	PRIMARY KEY(login),
	adresse NOT NULL
);
/
CREATE TYPE T_Categorie AS OBJECT
(
	nom VARCHAR2(50)
);
/
CREATE TABLE Categorie OF T_Categorie
(
	PRIMARY KEY(nom)
);
/
CREATE TYPE T_Modele AS OBJECT
(
	nom VARCHAR2(20)
);
/
CREATE TABLE Modele OF T_Modele
(
	PRIMARY KEY(nom)
);
/
CREATE TYPE T_Carburant AS OBJECT
(
	nom VARCHAR2(20)
);
/
CREATE TABLE Carburant OF T_Carburant
(
	PRIMARY KEY(nom)
);
/
CREATE TYPE T_Voiture AS OBJECT 
(
	numero_immatriculation VARCHAR2(50), 
    marque VARCHAR2(100), 
    couleur VARCHAR2(20), 
    annee_circulation NUMBER(4), 
    numero_contrat_assurance NUMBER(7), 
    descript VARCHAR2(255), 
    categorie REF T_Categorie, 
    modele REF T_Modele, 
    carburant REF T_Carburant
);
/
CREATE TABLE Voiture OF T_Voiture
(
	PRIMARY KEY(numero_immatriculation),
	SCOPE FOR (categorie) IS Categorie,
	SCOPE FOR (modele) IS Modele,
	SCOPE FOR (carburant) IS Carburant,
	categorie NOT NULL,
	modele NOT NULL,
	carburant NOT NULL
);
/
CREATE TYPE T_Option AS OBJECT
(
	nom VARCHAR2(50),
	descript VARCHAR2(200)
);
/
CREATE TABLE tOption OF T_Option
(
	PRIMARY KEY(nom)
);
/
CREATE TABLE Option_Voiture
(
	toption REF T_Option,
	SCOPE FOR (toption) IS tOption,
    voiture REF T_Voiture,
    SCOPE FOR (voiture) IS Voiture 
);
/
CREATE TYPE T_Pays AS OBJECT
(
	nom VARCHAR2(50),
	descript VARCHAR2(200)
);
/
CREATE TABLE Pays OF T_Pays
(
	PRIMARY KEY(nom)
);
/
CREATE TABLE Pays_Voiture
(
	pays REF T_Pays,
	SCOPE FOR (pays) IS Pays,
    voiture REF T_Voiture,
    SCOPE FOR (voiture) IS Voiture 
);
/
CREATE TABLE Annonce
(
	prix NUMBER(5,2), 
    date_dispo_start DATE, 
    date_dispo_fin DATE, 
    kilometrage NUMBER(7), 
    seuil_kilométrage NUMBER(7), 
    niveau_carburant NUMBER(5,2), 
    prix_kilometrage_supp NUMBER(5,2), 
    prix_litre_supp NUMBER(5,2), 
    proprietaire REF T_Proprietaire,
    SCOPE FOR (proprietaire) IS Proprietaire,
    voiture REF T_Voiture,
    SCOPE FOR (voiture) IS Voiture
);
/


CREATE TYPE T_Locataire AS OBJECT
(
    login VARCHAR2(20), 
    nom VARCHAR2(20), 
    prenom VARCHAR2(20),
    age NUMBER(3), 
    photo VARCHAR(200), 
    pseudo VARCHAR2(7),
    adresse  T_Adresse
) NOT FINAL;
/
CREATE TABLE Locataire OF T_Locataire
(
	PRIMARY KEY (login),
	adresse NOT NULL
);
/
CREATE TYPE T_Particulier UNDER T_Locataire
(
	numéro_permis NUMBER(7), 
    date_expiration DATE
);
/
CREATE TABLE Particulier OF T_Particulier
(
	PRIMARY KEY(login),
	adresse NOT NULL
);
/
CREATE TYPE T_Entreprise UNDER T_Locataire
(
	nomentreprise VARCHAR2(20),
    nb_salarié NUMBER(5)
);
/
CREATE TABLE Entreprise OF T_Entreprise
(
	PRIMARY KEY(login),
	adresse NOT NULL
);
/
CREATE TYPE T_Conducteur AS OBJECT
(
	nom VARCHAR2(10), 
    prénom VARCHAR2(10), 
    entreprise REF T_Entreprise
);
/
CREATE TABLE Conducteur OF T_Conducteur
(
	PRIMARY KEY (nom, prénom),
	SCOPE FOR (entreprise) IS Entreprise
);
/
CREATE TYPE T_Facture AS OBJECT
(
	numero_facture NUMBER(7), 
    kilometrage NUMBER(7), 
    datefacture DATE, 
    moyen_paiement VARCHAR2(10), 
    cout_horsforfait NUMBER(5,2),
    MEMBER FUNCTION Total RETURN NUMBER
);
/
CREATE TABLE Facture OF T_Facture
(
	PRIMARY KEY(numero_facture),
	CHECK (moyen_paiement='chèque' OR moyen_paiement='carte' OR moyen_paiement='liquide')
);
/
CREATE TYPE T_Contrat_Location AS OBJECT
(
	numéro_contrat NUMBER(7), 
    photo_véhicule VARCHAR2(100), 
    choix_franchises VARCHAR2(50), 
    Date_début DATE, 
    Date_fin DATE, 
    facture REF T_Facture, 
    particulier REF T_Particulier, 
    entreprise REF T_Entreprise,
    voiture REF T_Voiture
);
/


CREATE TABLE Contrat_Location OF T_Contrat_Location
(
	PRIMARY KEY (numéro_contrat),
	CHECK (choix_franchises='sans réduction' OR choix_franchises='franchise réduite' OR choix_franchises='zéro franchise' ),
	SCOPE FOR (facture) IS Facture,
	SCOPE FOR (particulier) IS Particulier,
	SCOPE FOR (entreprise) IS Entreprise,
    SCOPE FOR (voiture) IS Voiture,
	facture NOT NULL
);
/

CREATE TABLE Avis
(
    commentaire VARCHAR2(255), 
    note NUMBER(1), 
    signalement NUMBER(1), 
    contrat REF T_Contrat_Location,
    SCOPE FOR (contrat) IS Contrat_Location,
    locataire REF T_Locataire,
    SCOPE FOR (locataire) IS Locataire
);
/
CREATE TABLE Critere
(
	nom VARCHAR2(20) PRIMARY KEY, 
    check_in NUMBER(1), 
    check_out NUMBER(1), 
    photo VARCHAR2(200), 
    contrat REF T_Contrat_Location,
    SCOPE FOR (contrat) IS Contrat_Location
);
/


-- insert
INSERT INTO Proprietaire (login,nom,prenom,age,photo,pseudo,adresse) VALUES ('AntoineL','LEBRUN','Antoine',21,'https://xxxx.com/Antoine/photos-id.jpg','Antho',T_Adresse('Compiegne',60200,'rue de A'));
INSERT INTO Proprietaire (login,nom,prenom,age,photo,pseudo,adresse) VALUES ('GuillaumeR','RENAUD','Guillaume',22,'https://xxxx.com/Guillaume/photos-id.jpg','Guigui',T_Adresse('Paris',65001,'rue de B'));
/
INSERT INTO CATEGORIE (nom) VALUES
('4x4');
INSERT INTO CATEGORIE (nom) VALUES
('Sportive');
INSERT INTO CATEGORIE (nom) VALUES
('Citadine');
/
INSERT INTO MODELE (nom) VALUES
('C4');
INSERT INTO MODELE (nom) VALUES
('Serie 5');
INSERT INTO MODELE (nom) VALUES
('Duster');
INSERT INTO MODELE (nom) VALUES
('Mustang');
/
INSERT INTO CARBURANT (nom) VALUES
('SP95');
INSERT INTO CARBURANT (nom) VALUES
('Electrique');
INSERT INTO CARBURANT (nom) VALUES
('GPL');
/
DECLARE
    oidcar1 REF T_Carburant;
    oidcar2 REF T_Carburant;
    oidmod1 REF T_Modele;
    oidmod2 REF T_Modele;
    oidmod3 REF T_Modele;
    oidmod4 REF T_Modele;
    oidcat1 REF T_Categorie;
    oidcat2 REF T_Categorie;
    oidcat3 REF T_Categorie;
BEGIN
    SELECT REF(car) INTO oidcar1
    FROM Carburant car
    WHERE nom = 'SP95';
    
    SELECT REF(car) INTO oidcar2
    FROM Carburant car
    WHERE nom = 'GPL';
    
    SELECT REF(mod) INTO oidmod1
    FROM Modele mod
    WHERE nom = 'C4';
    
    SELECT REF(mod) INTO oidmod2
    FROM Modele mod
    WHERE nom = 'Duster';
    
    SELECT REF(mod) INTO oidmod3
    FROM Modele mod
    WHERE nom = 'Serie 5';
    
    SELECT REF(mod) INTO oidmod4
    FROM Modele mod
    WHERE nom = 'Mustang';
    
    SELECT REF(cat) INTO oidcat1
    FROM Categorie cat
    WHERE nom = 'Citadine';
    
    SELECT REF(cat) INTO oidcat2
    FROM Categorie cat
    WHERE nom = '4x4';
    
    SELECT REF(cat) INTO oidcat3
    FROM Categorie cat
    WHERE nom = 'Sportive';
       
    INSERT INTO VOITURE (numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, descript, modele, carburant, categorie) VALUES ('FF-277-CK', 'CITROEN', 'rouge', 1997, 100,'agyui', oidmod1, oidcar1, oidcat1);
    INSERT INTO VOITURE (numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, descript, modele, carburant, categorie) VALUES ('FF-238-CJ', 'Dacia', 'Noir', 2000, 101, 'asdf', oidmod2, oidcar1, oidcat2);
    INSERT INTO VOITURE (numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, descript, modele, carburant, categorie) VALUES ('FF-179-CK', 'BMW', 'Gris', 2001, 102,'huid', oidmod3, oidcar1, oidcat3);
    INSERT INTO VOITURE (numero_immatriculation, marque, couleur, annee_circulation, numero_contrat_assurance, descript, modele, carburant, categorie) VALUES ('FF-627-CJ', 'Ford', 'Noir', 1987, 103, 'sgycb', oidmod4, oidcar2, oidcat3);
END;
/
INSERT INTO tOPTION (nom, descript) VALUES
('GPS', 'gps');
INSERT INTO tOPTION (nom, descript) VALUES
('Siège chauffant', '2 sièges avant chauffant, 2 températures');
INSERT INTO tOPTION (nom, descript) VALUES
('Kit main libre', 'Kit main libre');
/
DECLARE
    oidopt1 REF T_Option;
    oidopt2 REF T_Option;
    oidvoi1 REF T_Voiture;
    oidvoi2 REF T_Voiture;
    oidvoi3 REF T_Voiture;
BEGIN
    SELECT REF(o) INTO oidopt1
    FROM tOption o
    WHERE nom ='GPS';
    
    SELECT REF(o) INTO oidopt2
    FROM tOption o
    WHERE nom ='Kit main libre';
    
    SELECT REF(v) INTO oidvoi1
    FROM Voiture v
    WHERE numero_immatriculation='FF-238-CJ';
    
    SELECT REF(v) INTO oidvoi2
    FROM Voiture v
    WHERE numero_immatriculation='FF-179-CK';
    
    SELECT REF(v) INTO oidvoi3
    FROM Voiture v
    WHERE numero_immatriculation='FF-277-CK';
    
    INSERT INTO Option_Voiture (toption, voiture) VALUES (oidopt1, oidvoi1);
    INSERT INTO Option_Voiture (toption, voiture) VALUES (oidopt1, oidvoi2);
    INSERT INTO Option_Voiture (toption, voiture) VALUES (oidopt1, oidvoi3);
END;
/
INSERT INTO PAYS (nom) VALUES
('France');
INSERT INTO PAYS (nom) VALUES
('Chine');
INSERT INTO PAYS (nom) VALUES
('Italie');
INSERT INTO PAYS (nom) VALUES
('Allemagne');
/
DECLARE
    oidpays1 REF T_Pays;
    oidpays2 REF T_Pays;
    oidpays3 REF T_Pays;
    oidpays4 REF T_Pays;
    oidvoi1 REF T_Voiture;
    oidvoi2 REF T_Voiture;
    oidvoi3 REF T_Voiture;
    oidvoi4 REF T_Voiture;
BEGIN
    SELECT REF(p) INTO oidpays1
    FROM Pays p
    WHERE nom = 'France';
    
    SELECT REF(p) INTO oidpays2
    FROM Pays p
    WHERE nom = 'Chine';
    
    SELECT REF(p) INTO oidpays3
    FROM Pays p
    WHERE nom = 'Italie';
    
    SELECT REF(p) INTO oidpays4
    FROM Pays p
    WHERE nom = 'Allemagne';
    
    SELECT REF(v) INTO oidvoi1
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-277-CK';
    
    SELECT REF(v) INTO oidvoi2
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-238-CJ';
    
    SELECT REF(v) INTO oidvoi3
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-179-CK';
    
    SELECT REF(v) INTO oidvoi4
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-627-CJ';
    
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays1,oidvoi1);
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays2,oidvoi1);
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays3,oidvoi1);
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays4,oidvoi2);
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays1,oidvoi3);
    INSERT INTO Pays_Voiture (pays,voiture) VALUES (oidpays1,oidvoi4);
END;
/
DECLARE
    oidpro1 REF T_Proprietaire;
    oidpro2 REF T_Proprietaire;
    oidvoi1 REF T_Voiture;
    oidvoi2 REF T_Voiture;
    oidvoi3 REF T_Voiture;
BEGIN
    SELECT REF(p) INTO oidpro1
    FROM Proprietaire p
    WHERE login ='AntoineL';
    
    SELECT REF(p) INTO oidpro2
    FROM Proprietaire p
    WHERE login ='GuillaumeR';
    
    SELECT REF(v) INTO oidvoi1
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-277-CK';
    
    SELECT REF(v) INTO oidvoi2
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-627-CJ';
    
    SELECT REF(v) INTO oidvoi3
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-179-CK';

    INSERT INTO ANNONCE (proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilométrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp) VALUES
(oidpro1, oidvoi1, 300.5, TO_DATE('2019-01-01', 'YYYY-MM-DD'), TO_DATE('2019-10-10', 'YYYY-MM-DD'), 122, 200, 0.67, 10.55, 23.27);
    INSERT INTO ANNONCE (proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilométrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp) VALUES
(oidpro2, oidvoi2, 500, TO_DATE('2019-03-06', 'YYYY-MM-DD'), TO_DATE('2019-06-07', 'YYYY-MM-DD'), 12546, 12873, 0.89, 20.35, 13.28);
    INSERT INTO ANNONCE (proprietaire, voiture, prix, date_dispo_start, date_dispo_fin, kilometrage, seuil_kilométrage, niveau_carburant, prix_kilometrage_supp, prix_litre_supp) VALUES
(oidpro2, oidvoi3, 200, TO_DATE('2018-01-01', 'YYYY-MM-DD'), TO_DATE('2018-07-07', 'YYYY-MM-DD'), 167, 189, 0.88, 25.45, 10.55);
END;
/
INSERT INTO LOCATAIRE (login,nom,prenom,age,photo,pseudo,adresse) VALUES ('HugoK','KESSLER','Hugo',23,'https://xxxx.com/Hugo/photos-id.jpg','Hug',T_Adresse('Lyon',69001,'rue de C'));
INSERT INTO LOCATAIRE (login,nom,prenom,age,photo,pseudo,adresse) VALUES ('FeierX','XIE','Feier',24,'https://xxxx.com/Feier/photos-id.jpg','Feifei',T_Adresse('Bordeaux',33100,'rue de D'));
INSERT INTO LOCATAIRE (login,pseudo,adresse) VALUES ('SG','Societe',T_Adresse('Toulouse',31000,'rue de E'));
INSERT INTO LOCATAIRE (login,pseudo,adresse) VALUES ('RN','Ren',T_Adresse('Paris',65001,'rue de F'));
/
INSERT INTO Particulier (login,nom,prenom,age,photo,pseudo,adresse,numéro_permis,date_expiration) VALUES ('HugoK','KESSLER','Hugo',23,'https://xxxx.com/Hugo/photos-id.jpg','Hug',T_Adresse('Lyon',69001,'rue de C'),9110912,TO_DATE('2020-03-03', 'YYYY-MM-DD'));
INSERT INTO Particulier (login,nom,prenom,age,photo,pseudo,adresse,numéro_permis,date_expiration) VALUES ('FeierX','XIE','Feier',24,'https://xxxx.com/Feier/photos-id.jpg','Feifei',T_Adresse('Bordeaux',33100,'rue de D'),9610928,TO_DATE('2023-07-13', 'YYYY-MM-DD'));
/
INSERT INTO Entreprise (login,pseudo,adresse,nomentreprise,nb_salarié) VALUES ('SG','Societe',T_Adresse('Toulouse',31000,'rue de E'),'Societe_General',200);
INSERT INTO Entreprise (login,pseudo,adresse,nomentreprise,nb_salarié) VALUES ('RN','Ren',T_Adresse('Paris',65001,'rue de F'),'Renault',250);
/
DECLARE
    oident1 REF T_Entreprise;
    oident2 REF T_Entreprise;
BEGIN   
    SELECT REF(e) INTO oident1
    FROM Entreprise e
    WHERE login = 'SG';
    
    SELECT REF(e) INTO oident2
    FROM Entreprise e
    WHERE login = 'RN';
    
    INSERT INTO Conducteur (nom, prénom, entreprise) VALUES ('HJS','Julie', oident1);
    INSERT INTO Conducteur (nom, prénom, entreprise) VALUES ('HUI','Clément', oident2);
END;
/
INSERT INTO FACTURE (numero_facture, kilometrage, datefacture, moyen_paiement, cout_horsforfait) VALUES (1, 145, TO_DATE('2018-03-15', 'YYYY-MM-DD'), 'chèque', 0);
INSERT INTO FACTURE (numero_facture, kilometrage, datefacture, moyen_paiement, cout_horsforfait) VALUES (2, 27839,TO_DATE('2018-07-08', 'YYYY-MM-DD') , 'carte', 10.5);
INSERT INTO FACTURE (numero_facture, kilometrage, datefacture, moyen_paiement, cout_horsforfait) VALUES (3, 178, TO_DATE('2018-11-12', 'YYYY-MM-DD'), 'liquide', 13.28);
/
DECLARE
    oidfac1 REF T_Facture;
    oidfac2 REF T_Facture;
    oidfac3 REF T_Facture;
    oidpar1 REF T_Particulier;
    oidpar2 REF T_Particulier;
    oident1 REF T_Entreprise;
    oidvoi1 REF T_Voiture;
    oidvoi2 REF T_Voiture;
    oidvoi3 REF T_Voiture;
BEGIN
    SELECT REF(f) INTO oidfac1
    FROM Facture f
    WHERE numero_facture=1;
    
    SELECT REF(f) INTO oidfac2
    FROM Facture f
    WHERE numero_facture=2;
    
    SELECT REF(f) INTO oidfac3
    FROM Facture f
    WHERE numero_facture=3;
    
    SELECT REF(p) INTO oidpar1
    FROM Particulier p
    WHERE login = 'HugoK';
    
    SELECT REF(p) INTO oidpar2
    FROM Particulier p
    WHERE login = 'FeierX';
    
    SELECT REF(e) INTO oident1
    FROM Entreprise e
    WHERE login = 'SG';
    
    SELECT REF(v) INTO oidvoi1
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-277-CK';
    
    SELECT REF(v) INTO oidvoi2
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-627-CJ';
    
    SELECT REF(v) INTO oidvoi3
    FROM Voiture v
    WHERE numero_immatriculation = 'FF-179-CK';
    
    INSERT INTO CONTRAT_LOCATION (numéro_contrat, photo_véhicule, choix_franchises, Date_début, Date_fin, facture, entreprise,voiture) VALUES
(2000, 'https://xxxx.com/voiture1/photos.jpg', 'sans réduction', TO_DATE('2018-03-04', 'YYYY-MM-DD'), TO_DATE('2018-03-15', 'YYYY-MM-DD'), oidfac1, oident1,oidvoi1);
    INSERT INTO CONTRAT_LOCATION (numéro_contrat, photo_véhicule, choix_franchises, Date_début, Date_fin, facture, particulier,voiture) VALUES
(2001, 'https://xxxx.com/voiture2/photos.jpg', 'franchise réduite', TO_DATE('2018-03-15', 'YYYY-MM-DD'), TO_DATE('2018-07-08', 'YYYY-MM-DD'), oidfac2, oidpar1,oidvoi2);
    INSERT INTO CONTRAT_LOCATION (numéro_contrat, photo_véhicule, choix_franchises, Date_début, Date_fin, facture, particulier, voiture) VALUES
(2002, 'https://xxxx.com/voiture3/photos.jpg', 'zéro franchise', TO_DATE('2018-07-08', 'YYYY-MM-DD'), TO_DATE('2018-11-12', 'YYYY-MM-DD'), oidfac3, oidpar2, oidvoi3);
END;
/
DECLARE 
    oidloc1 REF T_Locataire;
    oidloc2 REF T_Locataire;
    oidcon1 REF T_Contrat_Location;
    oidcon2 REF T_Contrat_Location;
BEGIN 
    SELECT REF(l) INTO oidloc1
    FROM Locataire l
    WHERE login = 'SG';
    
    SELECT REF(l) INTO oidloc2
    FROM Locataire l
    WHERE login = 'FeierX';
    
    SELECT REF(c) INTO oidcon1
    FROM Contrat_Location c
    WHERE numéro_contrat = 2000;
    
    SELECT REF(c) INTO oidcon2
    FROM Contrat_Location c
    WHERE numéro_contrat = 2001;
    
    INSERT INTO AVIS (contrat, locataire, commentaire, note, signalement) VALUES
(oidcon1, oidloc1, 'bien', 2, 1);
    INSERT INTO AVIS (contrat, locataire, commentaire, note, signalement) VALUES
(oidcon2, oidloc2, 'bon', 5, 0);
    INSERT INTO CRITERE (nom, check_in, check_out, photo, contrat) VALUES
('Siège', 1, 1, 'https://xxxx.com/critere1/photos.jpg', oidcon1);
INSERT INTO CRITERE (nom, check_in, check_out, photo, contrat) VALUES
('Volant', 1, 1, 'https://xxxx.com/critere2/photos.jpg', oidcon2);

END;

-- un fonction pour calculer prix total d'un facture
CREATE FUNCTION PrixTotal_facture (num_facture number) RETURN Number
IS
  prix_total NUMBER;
BEGIN
  SELECT c.facture.cout_horsforfait+a.prix INTO prix_total
  FROM Annonce a, Contrat_Location c
  Where a.voiture.numero_immatriculation = c.voiture.numero_immatriculation
  AND c.facture.numero_facture=num_facture; 
  RETURN prix_total;
END;

-- une vue pour afficher le prix de chaque facture
Create view facture_prix AS
Select f.numero_facture, PrixTotal_facture(f.numero_facture)
From Facture f;

--une procedure pour afficher les notes des voitures
CREATE OR REPLACE PROCEDURE pAffiche_NoteVoiture
IS
 CURSOR cNotevoiture IS
 SELECT a.contrat.voiture.numero_immatriculation, a.note
 FROM Avis a;
 voiture voiture.numero_immatriculation%TYPE;
 note avis.note%TYPE;
BEGIN
 OPEN cNotevoiture;
 LOOP FETCH cNotevoiture INTO voiture, note;
 EXIT WHEN cNotevoiture%NOTFOUND;
 DBMS_OUTPUT.PUT_LINE(voiture||' '||note);
 END LOOP;
END;
