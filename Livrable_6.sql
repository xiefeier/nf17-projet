-- code d’optimisation de la requête 1 :

create table VOITURE_FRANCE
 (
   VOITURE varchar
 );

insert into VOITURE_FRANCE
select voiture
from CONDUITE_AUTORISEE
where pays = 'France' ;


create table VOITURE_ITALIE
 (
   VOITURE varchar
 );

insert into VOITURE_ITALIE
select voiture
from CONDUITE_AUTORISEE
where pays = 'Italie' ;

create table VOITURE_CHINE
 (
   VOITURE varchar
 );

insert into VOITURE_CHINE
select voiture
from CONDUITE_AUTORISEE
where pays = 'Chine' ;

-- code d’optimisation de la requête 2 :

create view INFO_CONTRAT as
select c.numero_contrat, c.choix_franchises, c.voiture, cr.check_in, cr.check_out
from CONTRAT_LOCATION c, CRITERE cr
where c.numero_contrat = cr.contrat ;
