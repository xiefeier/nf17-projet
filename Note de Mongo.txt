Note de MongoDB

On a modifié une partie de notre UML pour intégrer des compositions (la partie en rouge dans image 'UML_NF17_modifie_MongoDB'). On a fait l'implémentation de ce sous-ensemble en Mongo. On a créé deux collections (Contrat et Locataire) et imbriqué les infos de 'facture' et 'critère' dans la collection 'Contrat'. Cette modification en Mongo nous permet de diminuer le nombre de tables, mais ça va perdre des contraintes de cardinalité et compliquer certaines requêtes

Les code de Mongo est dans le fichier 'Mongo_code', on a aussi proposé quelques requêtes en Mongo ci-dessous :

1. quels sont les numéros de contrats qui n'ont pas encore fait check_out de critère ?
db.Contrat.find({"critère.check_out": false}, {"numéro_contrat":1,"_id":0})

2. quels sont les numéros de contrats dont le kilométrage et supérieur à 1000 ?
db.Contrat.find({"facture.kilometrage":{$lt:2000}}, {"numéro_contrat":1, "_id":0})

3. quel est le locataire qui est associé au contrat numéro 2001 ?
db.Locataire.find({"avis.contrat.numero_contrat" : 2001}, {"nom":1, "prenom":1, "_id":0})

4. quels sont les locataires qui ont signalé au moins une voiture ?
db.Locataire.find({"avis.signalement" : true}, {"nom":1, "prenom":1, "_id":0})